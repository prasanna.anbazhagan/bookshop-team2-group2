package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {

    @RequestMapping("/application")
    public Book getBookName(@RequestParam(value="name", defaultValue="book") String name) {
        return new Book(name);
    }
}