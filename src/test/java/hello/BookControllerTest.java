package hello;

import org.junit.Assert;
import org.junit.Test;

public class BookControllerTest {

    @Test
    public void testControllerGivesBookInstance() {
        BookController bookController = new BookController();
        Assert.assertTrue(bookController.getBookName("hello") instanceof Book);
    }

    @Test
    public void testControllerCreatesBookShoWithGivenInput() {
        String input = "book";
        BookController bookController = new BookController();
        Assert.assertEquals(input, bookController.getBookName(input).getName());
    }
}
