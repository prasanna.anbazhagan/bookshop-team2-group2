FROM openjdk:8-alpine
ADD /build/libs/team2_group2-0.1.0.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]